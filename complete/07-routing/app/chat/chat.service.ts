import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

import { Room, Message, ChatRoom } from './model/chat';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ChatService {

  private readonly ROOMS_URL = 'http://localhost:8000/rooms';

  private rooms$;

  constructor(private http: HttpClient) {
    this.rooms$ = this.http.get<Room[]>(this.ROOMS_URL).share();
  }

  getRooms(): Observable<Room[]> {
    return this.rooms$;
  }

  getRoomById(roomId: number): Observable<ChatRoom> {
    return this.http.get<ChatRoom>(this.ROOMS_URL + '/' + roomId);
  }

  sendMessage(roomId: number, message: Message): Observable<any> {
    return this.http.post(this.ROOMS_URL + '/' + roomId, message);
  }

}
