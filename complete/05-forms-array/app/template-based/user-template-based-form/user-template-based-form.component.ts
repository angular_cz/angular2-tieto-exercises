import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { User, Contact } from '../../model/user';
import { cloneUser } from '../../model/clone';

@Component({
  selector: 'app-user-template-based-form',
  templateUrl: 'user-template-based-form.component.html',
  styleUrls: ['user-template-based-form.component.css']
})
export class UserTemplateBasedFormComponent {

  @Input('user') set userFromInput(userFromInput: User) {

    // TODO 1.6.1 - vytvořte kopii příchozího objektu
    this.user = cloneUser(userFromInput);
  };

  @Output() userSubmit = new EventEmitter<User>();

  user: User;

  formSubmit(form) {
    console.log('onSubmit', form);
    if (form.valid) {
      this.userSubmit.emit(this.user);
    }
  }

  addAddress() {
    this.user.contacts.push({} as Contact);
  }

  removeAddress(i: number) {
    this.user.contacts.splice(i, 1);
  }

}
